// Initialize your app
var myApp = new Framework7();

// Export selectors engine
var $$ = Dom7;


// Add view
var mainView = myApp.addView('.view-main', {
    // Because we use fixed-through navbar we can enable dynamic navbar
    dynamicNavbar: true
});


$$('.hide-navbar').on('click', function () {
    mainView.hideNavbar();
});

$$('.show-navbar').on('click', function () {
    mainView.showNavbar();
});

