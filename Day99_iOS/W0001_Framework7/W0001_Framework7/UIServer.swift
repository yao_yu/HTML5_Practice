//
//  UIServer.swift
//  xmgl_ios
//
//  Created by yao_yu on 15/2/4.
//  Copyright (c) 2015年 yao_yu. All rights reserved.
//

import Foundation

class WWWServer {
    
    var wwwRoot:String!
    var fileManager = NSFileManager.defaultManager()
    
    init(){
        wwwRoot = NSTemporaryDirectory().stringByAppendingPathComponent("xmgl_www")
        deployWWW()
    }
    
    deinit{
        if isFileExist(wwwRoot){
            fileManager.removeItemAtPath(wwwRoot, error: nil)
        }
    }
    
    func wwwPath(subPath:String) -> (Bool,String) {
        var path = wwwRoot.stringByAppendingPathComponent(subPath)
        return (isFileExist(path), path)
    }
    
    func isFileExist(fileName:String) -> Bool{
        return fileManager.fileExistsAtPath(fileName)
    }
    
    func deployWWW(){
        var srcfile = NSBundle.mainBundle().pathForResource("index", ofType: "html", inDirectory: "www")!
        var srcPath = srcfile.stringByDeletingLastPathComponent
        //wwwRoot = srcPath
        self.copyFrom(srcPath, to: wwwRoot)
    }
    
    func copyFrom(src: String, to dest: String){
        if !isFileExist(src){
            return
        }
        if isFileExist(dest){
            fileManager.removeItemAtPath(dest, error: nil)
        }
        fileManager.copyItemAtPath(src, toPath: dest, error: nil)
    }
}