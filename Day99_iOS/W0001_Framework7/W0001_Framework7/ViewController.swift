//
//  ViewController.swift
//  W0001_Framework7
//
//  Created by yao_yu on 15/2/5.
//  Copyright (c) 2015年 yao_yu. All rights reserved.
//


import UIKit
import WebKit

class ViewController: UIViewController {
    
    var webView:WKWebView!
    var www = WWWServer()
    
    func userContentController(userContentController: WKUserContentController, didReceiveScriptMessage message: WKScriptMessage) {
        var body:String = message.body["body"] as String!
        println(body)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupWebView()
        startLogin()
    }
    
    func setupWebView(){
        let userContentController = WKUserContentController()
        //userContentController.addScriptMessageHandler(self, name: "notification")
        
        let configuration = WKWebViewConfiguration()
        configuration.userContentController = userContentController
        webView = WKWebView(frame: self.view.bounds, configuration: configuration)
        webView.setTranslatesAutoresizingMaskIntoConstraints(false)
        self.view.addSubview(webView)
        
        let views = ["webView":self.webView]
        self.view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|[webView]|", options: NSLayoutFormatOptions(0), metrics: nil, views: views))
        self.view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|-0-[webView]|", options: NSLayoutFormatOptions(0), metrics: nil, views: views))
    }
    
    func startLogin(){
        var url = "index.html"
//        url = "framework7/examples/inline-pages/index.html"
        url = "index2.html"
        url = "bars_static_index.html"
        url = "bars_static_fixed_index.html"
        url = "bars_static_layout.html"
        url = "dynamic-navbar-fade.html"
        url = "searchbar.html"
        url = "sidepanels.html"
        url = "content-block.html"
        url = "grid.html"
        url = "alert-modal.html"
        url = "popup.html"
        url = "popover.html"
        url = "actionsheet.html"
        url = "index3.html"
        loadPage(url)
    }
    
    func loadPage(fileName:String){
        var (isfile,path) = www.wwwPath(fileName)
        if isfile{
            var url = NSURL(fileURLWithPath: path)
            var request = NSURLRequest(URL: url!)
            webView.loadRequest(request)
        } else {
            println(path + "不存在!")
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}
