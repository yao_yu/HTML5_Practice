//
//  ViewController.swift
//  S00001_wkwebview
//
//  Created by yao_yu on 15/1/25.
//  Copyright (c) 2015年 yao_yu. All rights reserved.
//

import UIKit
import WebKit

class NotificationScriptMessageHandler: NSObject, WKScriptMessageHandler {
    func userContentController(userContentController: WKUserContentController, didReceiveScriptMessage message: WKScriptMessage) {
        println(message.body["body"])
    }
}


class ViewController: UIViewController {

    var webview:WKWebView!
    
    var wwwRoot:String!
    
    func updateWWWFiles(){
        var fileManager = NSFileManager.defaultManager()
        var path:NSString = NSBundle.mainBundle().pathForResource("index", ofType: "htm", inDirectory: "html")!
        var srcPath = path.stringByDeletingLastPathComponent

        self.copyFrom(srcPath, to: wwwRoot)
    }
    
    func wwwFile(subPath:String) -> String {
        return wwwRoot.stringByAppendingPathComponent(subPath)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        var _bounds = self.view.bounds
        wwwRoot = NSTemporaryDirectory() + "/www"

        updateWWWFiles()
        
//        let source = "document.body.style.background = \"#777\";"
//        let userScript = WKUserScript(source: source, injectionTime: WKUserScriptInjectionTime.AtDocumentEnd, forMainFrameOnly: true)
        
        let userContentController = WKUserContentController()
//        userContentController.addUserScript(userScript)
        let handler = NotificationScriptMessageHandler()
        userContentController.addScriptMessageHandler(handler, name: "notification")
        
        let configuration = WKWebViewConfiguration()
        configuration.userContentController = userContentController
        webview = WKWebView(frame: self.view.bounds, configuration: configuration)
        webview.setTranslatesAutoresizingMaskIntoConstraints(false)
        view.addSubview(webview)
        
        
        let views = ["MyLabel":self.webview]
        self.view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|[MyLabel]|", options: NSLayoutFormatOptions(0), metrics: nil, views: views))
        self.view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|-[MyLabel]-|", options: NSLayoutFormatOptions(0), metrics: nil, views: views))

        
        var sFile:String
        sFile = "S0001_layout.htm"
        sFile = "index.htm"
        sFile = "login.html"
        sFile = "sidebar.html"
        sFile = "blog.html"
        sFile = "widget.html"
        sFile = "D09_pure_js.htm"
        sFile = "D09_0002_no_jquery.htm"
        sFile = "index.htm"
        sFile = "dist/index.html"
//        sFile = "Sticky Footer Navbar Template for Bootstrap.html"
//        var path = wwwFile()
        var path = wwwFile(sFile)
        var url = NSURL(fileURLWithPath: path)
        var sUrl = "http://dearb.me/hi/build/"
        url = NSURL(string: sUrl)
        var request = NSURLRequest(URL: url!)
        webview.loadRequest(request)
        
//        webview.loadHTMLString("<!doctype html><html><head><meta charset='utf-8'/><title>测试</title></head><body><br/><h1>这是一个测试</h1><button type='button' onclick=\"window.webkit.messageHandlers.notification.postMessage({body: '中华人民共和国'});\">Click Me!</button> </body></html>", baseURL: nil)
        

//        let button: UIButton = UIButton.buttonWithType(UIButtonType.System) as UIButton
//        button.setTitle("测试", forState: UIControlState.Normal)
//        button.frame = CGRectMake(100, 200, 100, 30)
//        view.addSubview(button)
//        
//        button.addTarget(self, action: "onclick", forControlEvents: UIControlEvents.TouchUpInside)
       
    }
    
    func onclick(){

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func copyFrom(src : String, to dest : String){
        var fileManager = NSFileManager.defaultManager()
        if !fileManager.fileExistsAtPath(src){
            return
        }
        if fileManager.fileExistsAtPath(dest){
            fileManager.removeItemAtPath(dest, error: nil)
        }
        fileManager.copyItemAtPath(src, toPath: dest, error: nil)
    }


}

