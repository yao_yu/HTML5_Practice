//
//  ViewController.swift
//  html_canvas
//
//  Created by yao_yu on 14/12/23.
//  Copyright (c) 2014年 yao_yu. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    var webView:UIWebView!
    var myButton:UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()

        webView = UIWebView(frame: self.view.frame)
        self.view.addSubview(webView)
        webView.setTranslatesAutoresizingMaskIntoConstraints(false)


        let views = ["MyLabel":webView]
        self.view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("|[MyLabel]|", options: NSLayoutFormatOptions(0), metrics: nil, views: views))
        self.view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|-[MyLabel]-|", options: NSLayoutFormatOptions(0), metrics: nil, views: views))
        
        
        
        var path = NSBundle.mainBundle().pathForResource("S0006_phone_page", ofType: "htm", inDirectory: "html")
        var url = NSURL(fileURLWithPath: path!)
        url = NSURL(string: "http://html5test.com")
        var request = NSURLRequest(URL: url!)

        webView.loadRequest(request)
        
//        myButton = UIButton.buttonWithType(UIButtonType.System) as UIButton
//        myButton.frame = CGRectMake(20, 200, 100, 20)
//        myButton.setTitle("确定", forState: UIControlState.Normal)
//        myButton.addTarget(self, action: "onOK", forControlEvents: UIControlEvents.TouchUpInside)
//        webView.addSubview(myButton)
        
    }
    
    func onOK(){
        println(webView.stringByEvaluatingJavaScriptFromString("document.getElementById('a').value;"))
        println(webView.stringByEvaluatingJavaScriptFromString("document.getElementById('b').value;"))
        var result = webView.stringByEvaluatingJavaScriptFromString("document.getElementById('a').value + document.getElementById('b').value;")
        println(result)
        var b = result!
        webView.stringByEvaluatingJavaScriptFromString("document.getElementById('c').value = '" + result! + "'")
        println(webView.stringByEvaluatingJavaScriptFromString("document.getElementById('c').value;"))
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

