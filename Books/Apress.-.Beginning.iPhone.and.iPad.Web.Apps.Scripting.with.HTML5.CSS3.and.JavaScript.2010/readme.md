<code>
Contents
About the Authors
About the Technical Reviewer
Acknowledgments
Introduction
</code>
####Part I: Getting Started with Web App Development
#####C1: Development Tools 														3
#####C2: Development Environment												13
#####C3: Introducing Developer and Debugging Tools								29

####Part II: Web App Design with HTML5 and CSS3
#####C4: The Anatomy of a Web Application										67
#####C5: User Experience and Interface Guidelines									89
#####C6: Interesting CSS Fetures for Your Web Application User Interface				117
#####C7: Bitmap and Vector Graphics and Downloadable Fonts with Canvas and SVG	165
#####C8: Embedding Audio and Video Content in Your Web Application				219
#####C9: Handling Transformations, Animations, and Special Effects with CSS			257

####Part III: Going Futher with JavaScript and Web Standards
#####C10: An Object-Oriented JavaScript Programming Primer						301
#####C11: Cross-Document Communication											321
#####C12: Ajax and Dynamic Content												343
#####C13: Using Touch and Gesture Events											367
#####C14: Location-Aware Web Applications											397
#####C15: A Better Handling of Client-Side Data Storage								431
#####C16: 