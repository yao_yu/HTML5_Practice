##新手学HTML5移动开发
###第1章 HMTL介绍
#### 关键技术与概念
* HTML标签和属性
<pre>
	!DOCTYPE HTML
	html
		head
			title
			meta
			link rel='stylesheet' href='style.css' type='text/css'
			script src=''
		body
			div
	<!-- 注释 -->
</pre>
* 合并样式表和JavaScript
* 标题和段落
	h1...h6,p
* 字体,强调方式和颜色
	b,big,del,em,i,small,sub,sup
	font color,face,size
* 内嵌图片和超链接
	img src= alt= width= height= border= align=
	a href= target= name=
* 表, 列表和表单
	table,th,th,td
	ul,ol,li
	form
* 使用div和span标签
* 帧集及iframe

### 第4章 CSS3的增强特性
#### 关键技术与概念
* 背景和边框样式,颜色和图片
* 创建盒子与文本阴影
* 元素溢出
* 处理颜色与改变透明度
* 单词另起一行与列的使用
* 盒子大小与调整大小
* 使用Web字体